package multidownload

import (
	"fmt"
	"github.com/levigross/grequests"
	"os"
	"regexp"
	"strconv"
	"sync"
)

var DEBUG bool = false

func Download(url string, threads int, filename string, debug bool)  {
	threads = threads-1
	DEBUG = debug
	canMultithread, leng := parseHeader(url)
	if len(filename) < 1 {
		r := regexp.MustCompile(".+/(.+)")
		filename = r.FindStringSubmatch(url)[1]
	}
	debugPrint(filename)
	debugPrint(canMultithread)
	debugPrint(leng)
	var each int = leng / threads
	debugPrint(each)
	var wg sync.WaitGroup = sync.WaitGroup{}
	for i := 0; i < leng; i+=each {
		wg.Add(1)
		go downloadPart(&wg, url, filename, i, i+each)
	}
	wg.Wait()
	os.Create(filename)
	for i := 0; i < threads + 1; i++ {
		addTogather(filename, i)
	}
}

func downloadPart(wg *sync.WaitGroup, url string, filename string, start, end int) {
	defer wg.Done()
	debugPrint(fmt.Sprintf("Downloading %d to %d | part %d", start, end, start/(end-start)))
	ops := &grequests.RequestOptions{
		Headers: map[string]string{"Range": fmt.Sprintf("bytes=%d-%d", start, end-1)},
	}
	res, _ := grequests.Get(url, ops)
	if err := res.DownloadToFile(fmt.Sprintf("%s.part.%d", filename, start/(end-start))); err != nil {
		fmt.Println(err)
	}
}

func parseHeader(url string) (bool, int) {
	res, _ := grequests.Get(url, nil)
	ar := res.Header.Get("Accept-Ranges")
	leng, _ := strconv.Atoi(res.Header.Get("Content-Length"))
	if string(ar) != "bytes" {
		return false, 0
	}
	return true, leng
}

func debugPrint(a ...interface{}) {
	if DEBUG {
		fmt.Println(a)
	}
}

func addTogather(filename string, partnum int) {
	debugPrint(fmt.Sprintf("combining part: %d", partnum))
	finalfile, _ := os.OpenFile(filename, os.O_RDWR|os.O_APPEND, os.ModeAppend)

	partfile, _ := os.Open(fmt.Sprintf("%s.part.%d", filename, partnum))

	defer finalfile.Close()
	defer partfile.Close()
	defer os.Remove(fmt.Sprintf("%s.part.%d", filename, partnum))

	buffer := make([]byte, 1024)
	for {
		n, err := partfile.Read(buffer)
		if err != nil {
			fmt.Println(err)
		}
		if n == 0 {
			break
		}
		if _ , err = finalfile.Write(buffer[:n]); err !=nil {
			fmt.Println(err)
		}
	}
}
