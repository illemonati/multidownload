package main

import (
	"flag"
	"fmt"
	"gitlab.com/illemonati/multidownload/modules/multidownload"
)

var url string
var threads int
var filename string

func init() {
	flag.StringVar(&url, "u", "", "url to download")
	flag.IntVar(&threads, "t", 8, "threads to use")
	flag.StringVar(&filename, "o", "", "filename to save as")
	flag.Parse()
}

func main() {
	fmt.Printf("Downloading %s with %d threads\n", url, threads)
	multidownload.Download(url, threads, "", true)
}




